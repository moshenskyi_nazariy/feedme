package com.example.nazariy.places.domain.entities.details;

public class Response {
    private Venue venue;

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

}
