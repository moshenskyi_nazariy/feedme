package com.example.nazariy.places.domain.entities.details;

import java.util.List;

public class Lists {

    private List<Group> groups = null;

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

}
