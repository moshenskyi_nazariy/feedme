package com.example.nazariy.places.domain.entities.details;

public class Likes {

    private int count;
    private String summary;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

}
