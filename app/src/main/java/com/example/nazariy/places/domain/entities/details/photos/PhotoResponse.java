package com.example.nazariy.places.domain.entities.details.photos;


public class PhotoResponse {

    private Photos photos;

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }

}
