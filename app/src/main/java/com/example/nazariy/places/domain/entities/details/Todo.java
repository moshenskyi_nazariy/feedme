package com.example.nazariy.places.domain.entities.details;

public class Todo {

    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
