package com.example.nazariy.places.domain.entities.details;

public class PageItem {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
